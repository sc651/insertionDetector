# Script to handle execution of KID program
# sets up initial directory structure, tests input files exist to prevent downstream errors

import argparse, os, json
from scripts.ParseKI import *           # One ParseKI object per pool - extracts relevant reads used downstream analysis
from scripts.parseClusters import *     # Handles raw k-means clustering followed by modified z-score outlier detection and output of raw clusters
from scripts.isolateClips import *      # Finds soft clipped and ahrd clipped reads and generates a fastq file for realignment
from scripts.realignClips import *      # Realigns the soft clipped reads
from scripts.outputRawClips import *    # Output raw read data which can be used for manual curation, output summary of annotated clip data
from scripts.summariseRawClips import * # Group clipped locations by distance (2kb) and output for annotation
from scripts.annotateClips import *     # Output closest 2 features on each strand, also can filter for closest two
from scripts.obtainProteinSeq import *  # Get the pepetide sequences based on annotations
from scripts.borderSeqs import *        # Outputs border seq fasta file fo reach unannotated insertion
 
def test_inputs(bam_file_dict=None, args=None): #region, bam):
    """ Quick tests of input file locations """
    if args:
        assert (isinstance(args.region, str)), "Please provide known insertion chromosome as a string"  # KI chormosome must be string
        assert (os.path.isfile(args.bam_list)), "The bam file list " + args.bam_list + " cannot be located. Check filepath is correct"  # Check bam file list is present
        assert (os.path.isfile(args.reference)), "Te reference file " + args.reference + " Cannot be located. check filepath is corret" 
    elif bam_file_dict:
        assert(os.path.isfile(bam_file_dict['bam'])), "The bam file " + bam_file_dict['bam'] + " cannot be located. Check filepath is correct"  # Test bam is present
        assert(os.path.isfile(bam_file_dict['fq1'])), "The bam file " + bam_file_dict['fq1'] + " cannot be located. Check filepath is correct"  # Test fq1 is present      
        assert(os.path.isfile(bam_file_dict['fq2'])), "The bam file " + bam_file_dict['fq2'] + " cannot be located. Check filepath is correct"  # Test fq2 is present 
        
def setup_dir():
    """ Add directory structure if not present"""
    cwd = os.getcwd()
    if not os.path.exists(cwd + "/outputs"):        # If first time running in this location
        os.makedirs(cwd + "/outputs/re_alignment")  # Realignment data files to be futher separated by pool
        os.makedirs(cwd + "/outputs/insertions")    # Final outputs of annotated insertion locations
        os.makedirs(cwd + "/outputs/clustering")    # PE clustering files to be further separated by pool
        os.makedirs(cwd + "/outputs/clipped_reads") # Ouptut of clipped read summary, not spearated by pool
        os.makedirs(cwd + "/outputs/json_objects")  # Serialise each object to json - doesn't serialise the pysam objects
        os.makedirs(cwd + "/outputs/logs")
        
def bam_to_dict(bam_list):
    """ Takes bam file list and returns dict { 'pool' : 'path/to/bam' } """
    bam_dict = {}
    with open(bam_list, 'r') as bams:
        for line in bams:
            spl = line.strip('\n').split('\t')
            bam_dict[spl[1]] = {"bam" : spl[0], "fq1" : spl[2], "fq2" : spl[3]}
    return bam_dict
    
def output_obj(KI_obj):
    KI_obj.allRegionReads = []     
    KI_obj.pairedRegionReads = []  
    KI_obj.interRegionReads = []   
    KI_obj.rePairedInterRegionReads = []
    KI_obj.annotation_df = KI_obj.annotation_df.__dict__
    with open('test.json', 'w') as test_f:
        s = json.dump(KI_obj.__dict__, test_f)
    
def main(args):# bam_list, region):
    """ Excecution handler """
    if args.borders:
        run_nucleotide_extraction(args)
        print(args.insertions)
    else:
       all_KI_objects = []     # Use this to retain all 
       
       test_inputs(args=args)  # Test file paths of intputs are all correct
       setup_dir()             # Add directory structure if requried 
       bam_list_dict = bam_to_dict(args.bam_list)  # Convert bam list to Dictionary
       for pool, bam_dict in bam_list_dict.items():     # Test each bam file is present on system
           test_inputs(bam_file_dict=bam_dict)
       for pool, bam_dict in bam_list_dict.items():     # Iterate through bam files
           #### Extract and categorise read pairs for analysis ####
           KI_obj = ParseKI(pool, bam_dict['bam'], bam_dict['fq1'], bam_dict['fq2'], args.region)  # ParseKI object created for each pooled sample. Is subsequently, incrementally updated.
           
           #### Paired End k-means Clustering ####
           KI_obj.cluster_levels = kClusters(KI_obj, 0)        # Generate first round of K clusters and modz score sub clusters. 0 is cluster level
           writeClusters(KI_obj)                               # Writes inital clusters to file for consideration with
           
           #### clipped read re_mapping ####
           KI_obj.KI_hard_clip_fq, KI_obj.KI_soft_clip_fq = isolate_clips(KI_obj)   # Return  bam file of clipped reads from isolateClips.py
           KI_obj.realigned_bam = realign_clips(KI_obj, args.reference) # Return realigned bam file from realignClips.py
           
           #### output raw, unannotated sc bed ###
           KI_obj.realigned_raw_reads = output_raw_clips(KI_obj) # Output raw reads and insertion bed file
           KI_obj.unannotated_bed = output_unannotated_bed(KI_obj)
           print(KI_obj.unannotated_bed)
               
           #### Integrate clipped read remapping and PE calls ####
           
           #### Annotate_realigned ####
           if args.gff:
               KI_obj.annotated_bed = output_annotated_bed(KI_obj, args.gff)   # Annotate with gff if gff provided output annotated, filtered bed file
               
               #### Obtain protein sequences ####
               KI_obj.annotation_df = obtain_peptides(KI_obj)                  # If annotated get ensembl protein sequences from gene annotations and filters annotation
               KI_obj.annotation_df = filter_annotated(KI_obj)                 # Filter annotated df before final output, replace previously annotated be but dont need to return as path is same
               KI_obj.peptide_fasta = ouput_filtered_peptides(KI_obj)          # Output protien seqs to fasta
               KI_obj.insertion_file = output_filtered_annotated_clips(KI_obj) # Output a summary of the annoated df
               
               #### Output border seqs ####
               #KI_obj.border_seqs = border_extracton(KI_obj)
               
               #### Output serialised data ####
               #output_obj(KI_obj)
           
    
           else:
               print('No annotation file provided. Skipping annotation. Skipping border extraction.')
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--bam_list", help="Input bam file list, see example_data/input_bam_list. e.g. <path/to/bam>\t<Pool_number>")
    parser.add_argument("-r", "--region", help="The KID chromosome name")
    parser.add_argument("-fa", "--reference", help="Realignment reference file")
    parser.add_argument("-gff", "--gff", nargs='?', help="Annotation file in gff format")
    parser.add_argument("--borders", help='Run output of nucleotide sequence for primer design', action='store_true')
    parser.add_argument("--insertions", nargs='?', help='Insertions needing primers designed')
    args = parser.parse_args()                                         
    main(args)
