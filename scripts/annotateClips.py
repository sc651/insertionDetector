# Uses bed tools closest bed to annotate based on two closest features on each strand
# Outputs bed file whcih is used to gather peptide sequences
# Filter annotated later used to decrease quantitiy of calls

import pandas as pd
import subprocess, os
        
def output_annotated_bed(KI_obj, gff):
    """ Returns two closest annotations from each strand based on insertio bed file """
    annotated = bed_closest_gff(KI_obj.unannotated_bed, gff)
    return output_annotations(KI_obj, annotated)
    
def bed_closest_gff(alignment_bed, gff_file):
    """ First looks for direct intersects, if there are none looks for the closest each way on both strands """
    sorted = alignment_bed + '.sorted'                  # Sorted bed file name 
    with open(sorted, 'w') as sort_file:                # Open softed bed file for writing
        cmd = ['bedtools', 'sort', '-i', alignment_bed] # Sort bed file command
        subprocess.call(cmd, stdout=sort_file)          # Output sorted command to sroted file
    annotated = sorted + '.annotated'                   # Annotated bed file name
    with open(annotated, 'w') as ouf:                   # Open annotated file name
        cmd = ['bedtools', 'closest', '-a', sorted, '-b', gff_file, '-s', '-k', '2', '-D', 'ref'] # Bedtools closest 2 on each strand up stream is negative (lower than reference)
        subprocess.call(cmd, stdout=ouf)                # Output intersections to annotation file
    os.remove(sorted)                                   # Remove intemediate file
    return annotated
              
def output_annotations(KI_obj, annotated):
    """ Adds headers to intersect output include gene annotations """
    out = os.getcwd() + '/outputs/clipped_reads/' + KI_obj.pool + '/' + KI_obj.pool + '.annotated.filtered.bed'
    df = pd.read_csv(annotated, sep='\t', names=['chrom', 'min_clip_pos', 'max_clip_pos', 'clip_range', 'clip_count', 'strand', 'classifier', \
                   'feat_chrom', 'annotator_database', 'feat_type', 'feat_start', 'feat_stop', 'n', 'feat_strand',\
                   'n2', 'feat_annotation', 'distance_from_insertion'])
    df.to_csv(out, sep='\t', index=False)
    os.remove(annotated)        # Remove the un peptide annotated df
    return out
    
def filter_annotated(KI_obj):
    """ Choose the closest feature on each strand which
        is a greater start than insertion max on plus strand (promoter)
        is a lesser start than insertion min in negative strand (promoter) """
    clipped_site_groups = KI_obj.annotation_df.groupby(['chrom','min_clip_pos','max_clip_pos'])     # Group annotated by chrom, start, stop
    keeper_indicies = []    # Indicies of rows to keep 
    for name, group in clipped_site_groups:         # Iterate through the grouped annotations
        strand_groups = group.groupby(['strand'])   # Re_group by strand
        this_feature_indicies = []
        for sub_name, strand_group in strand_groups:
            group_filter = strand_group_filter(strand_group)            # Exception here where some features filtered out because
            keeper_indicies.append(group_filter)                        # For each strand group, return the keeper rows
            this_feature_indicies.append(group_filter)
        if len([item for sublist in this_feature_indicies for item in sublist]) == 0:   # Then feature logic annotates them both as not in promotor
            for sub_name, strand_group in strand_groups:                                # Return closest feature for each strand
                keeper_indicies.append(strand_group_exception(strand_group))
    keeper_indicies = sorted([item for sublist in keeper_indicies for item in sublist])
    filtered_df = KI_obj.annotation_df.iloc[keeper_indicies]
    with open(KI_obj.annotated_bed, 'w') as ouf:
        filtered_df.to_csv(ouf, sep='\t')
    return filtered_df
    
def strand_group_exception(strand_group):
    """ Captures exceptions where insertion not in promotor of each strand so returns closest for each strand
        May need to work in so it returns cosest if not two"""
    return [strand_group.distance_from_insertion.abs().idxmin()] 
    
def strand_group_filter(strand_group):
    """ Retain the required feature """
    strand_group_keepers = []                                           # Used to append indicies of rows to keep after iterating over all, will update annotated df based on these indicies
    if strand_group['strand'].iloc[0] == '+':                           # Check if is plus strand
        if len(strand_group) == 2:                                      # Check if there are two
            for idx, row in strand_group.iterrows():                    # Iterate through each row in the group of two positive strand
                if row['distance_from_insertion'] == 0:                 # If it's in a gene
                    strand_group_keepers.append(idx)                    # Keep index if the distance is 0
            if len(strand_group_keepers) == 1:                          # Keep if only one form this strand is in a gene
                return strand_group_keepers                             # return that index for this group of two
            elif len(strand_group_keepers) == 0:                        # if len 0 then none in a gene so do distance checek
                lookup_distance = []                                    # Place to do a lookup check
                for idx, row in strand_group.iterrows():                # Iterate through both rows
                    if row['feat_start'] > row['max_clip_pos']:         # Then the insertion is before the ORF start on this strand
                        lookup_distance.append([idx,row])               # Append data for lookup later        
                if len(lookup_distance) == 1:                           # If one then only one of two closest is upstream so return index of the first
                    strand_group_keepers.append(lookup_distance[0][0])  # Return index
                    return strand_group_keepers
                elif len(lookup_distance) == 2:                         # Then the two closest are sequentially 'after' the insertion so keep closest
                    strand_group_keepers.append(strand_group.loc[strand_group['distance_from_insertion'].idxmin()].name) # Append the index/name from the series w/ shortest distance
            return strand_group_keepers
        else:
            print('Three annotations here', '/n')# strand_group)
            return strand_group_keepers
    elif strand_group['strand'].iloc[0] == '-':                         # If minus strand
        if len(strand_group) == 2:
            for idx, row in strand_group.iterrows():                    # Iterate through each row in the group of two positive strand
                if row['distance_from_insertion'] == 0:                 # If it's in a gene
                    strand_group_keepers.append(idx)                    # Keep index if the distance is 0
            if len(strand_group_keepers) == 1:                          # Keep if only one form this strand is in a gene
                return strand_group_keepers                             # return that index for this group of two
            elif len(strand_group_keepers) == 0:                        # if len 0 then none in a gene so do distance checek
                lookup_distance = []                                    # Place to do a lookup check
                for idx, row in strand_group.iterrows():                # Iterate through both rows
                    if row['feat_stop'] < row['min_clip_pos']:          # Then the insertion is before the ORF start on this strand
                        lookup_distance.append([idx,row])               # Append data for lookup later 
                if len(lookup_distance) == 1:                           # If one then only one of two closest is upstream so return index
                    strand_group_keepers.append(lookup_distance[0][0])  # Return index
                    return strand_group_keepers
                elif len(lookup_distance) == 2:                         # Then the two closest are sequentially 'after' the insertion so keep closest
                    strand_group_keepers.append(strand_group.loc[strand_group['distance_from_insertion'].idxmax()].name)  # Max here as negative
            return strand_group_keepers
        else:
            print('Three annotations here', '/n')
            return []
    else:
        print('no strand info - exception in annotate_clips strand_group_filter')
    
    