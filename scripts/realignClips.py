import pysam, os, subprocess

def realign_clips(KI_obj, reference):
    """ Take take combine soft clipped and hard clipped reads for realignment """
    print("starting realignment")
    cwd = os.getcwd()
    realigned_md_bam = cwd + '/outputs/re_alignment/' + KI_obj.pool + '/' + KI_obj.pool + '_clip_region_rln.md.bam'
    if os.path.isfile(realigned_md_bam):    # If pool has already been realigned, don't do it again. Maybe remove this after testing complete.
        return realigned_md_bam
    else:
        fq_root, ext = os.path.splitext(KI_obj.KI_soft_clip_fq) # Isolating the root bam file to in
        realigned_sam = fq_root + '_rln.sam'                    # Intemeditae sam file in realignment
        realigned_bam = fq_root + '_rln.bam'                    # Intemediate bam file in realignment
        realigned_md_bam = fq_root + '_rln.md.bam'              # Final bam file in realignment
        metrics = fq_root + '.md_metrics'                       # Mark duplicates metrics file
        #bowtie2(fq, realigned_sam) 
        #bwa_aln(fq, realigned_sai, reference)
        #bwa_samse(fq, realigned_sai, reference, realigned_sam)
        fq = combine_fq(KI_obj, fq_root)
        bwa(fq, realigned_sam, reference)
        sam_to_bam(realigned_sam, realigned_bam)
        mark_duplicates(realigned_bam, realigned_md_bam, metrics)
        remove_intemediates(fq, realigned_sam, realigned_bam) 
    return realigned_md_bam
    
def combine_fq(KI_obj, fq_root):
    temp_fq = fq_root + '_temp.fq'
    with open(temp_fq, 'w') as out_file:
        subprocess.call(['cat', KI_obj.KI_soft_clip_fq, KI_obj.KI_hard_clip_fq], stdout=out_file)
    subprocess.call(['rm', KI_obj.KI_soft_clip_fq, KI_obj.KI_hard_clip_fq])
    return temp_fq

def bowtie2(fq, realigned_sam):
    idx = "/gpfs/ts0/projects/Research_Project-184686/publications/insertionDetector/bow_idx/ustil.fa"
    subprocess.call(['bowtie2', '-x', idx, '-U', fq, '-S', realigned_sam, '--local'])
    
def bwa_aln(fq, realigned_sai, reference):
    with open(realigned_sai, 'w') as sai:
        subprocess.call(['bwa', 'aln', reference, fq], stdout=sai)

def bwa_samse(fq, realigned_sai, reference, realigned_sam):
    with open(realigned_sam, 'w') as sam:
        subprocess.call(['bwa', 'samse', reference, realigned_sai, fq, '-n', '10'], stdout=sam)
 
def bwa(fq, realigned_sam, reference):
    """ Alingns fastq to reference using bwa - should be reference without known insertion region """
    with open(realigned_sam, 'w') as sam:
        subprocess.call(['bwa', 'mem', reference, fq,], stdout=sam)
        
def sam_to_bam(sam, bam):
    """ sorts sam file and outputs bam file """
    subprocess.call(['java', '-jar', '/gpfs/ts0/projects/Research_Project-184686/software/picard/build/libs/picard.jar', \
                     'SortSam', 'INPUT=' + sam, 'OUTPUT=' + bam, 'SORT_ORDER=coordinate'])
    
def mark_duplicates(bam, md_bam, metrics):
    subprocess.call(['java', '-jar', '/gpfs/ts0/projects/Research_Project-184686/software/picard/build/libs/picard.jar', \
                     'MarkDuplicates', 'I='+bam, 'O='+md_bam, 'M='+metrics, "REMOVE_DUPLICATES=TRUE", "CREATE_INDEX=TRUE"])
    
def remove_intemediates(*args):
    """ REmove intemediat efiles form the system """
    for i in args:
        subprocess.call(['rm', i])

