import sys

#largest gap in array - if there are no clips use middlemost base of either side:
#values = [1,2,3,7,8,9]
#max(abs(x - y) for (x, y) in zip(values[1:], values[:-1]))

class CategoriseClusterPair():
    id = ""
    cluster_instance = ""
    K1_data = ""
    K2_data = ""
        
    #Top level - cluster pair filter status
    filter = False         #states whether it is filtered or not 
    filter_code = ""

    #Identify which reads to use see filter codes for exclusion reason
    K1 = ""                #cluster 1 (c1 previously)
    K1z1 = ""              #cluster one sub cluster 1
    K1z2 = ""              #clsuter two subcluster 2
    K2 = ""                #cluster 2 (c2 previously)
    K2z1 = ""              #cluster one sub cluster 1 
    K2z2 = ""              #cluster two sub cluster 2

    #categorise K means clusters to determine necessity of sub cluster analysis
    kclust_diff_cat = ""       #difference category (proportional differences in RC between pairs)
    kclust_SD_cat = ""         #SD of normaliesed cluster pair values category 
    kclust_dist_cat = ""       #distance category K1 and K2 as prefixes    

    #categorisation of sub clusters S1, S2 of K1 - only required if cannot categorise based on K clusters alone
    K1subclust_diff_cat = ""
    K1subclust_SD_cat = ""
    K1subclust_dist_cat = ""
        
    #categorisation of sub clusters S1, S2 of K2 - only required if cannot categorise based on K clusters alone
    K2subclust_diff_cat = ""
    K2subclust_SD_cat = ""
    K2subclust_dist_cat = ""        
    
    def __init__(self, cluster_instance, id):
        """ Generate prediction of insertion status - each cluster pair has modified z score subsets of itself
            can be used to the classify based on booleans returned """
        self.id = id                                #integer id indicates first clustering. Floating indicates re-cluster n e.g. 1.3 would be third re-cluster of cluster pair 1
        self.cluster_instance = cluster_instance    #Cluster pair instance - 6 options 
        self.prediction = ""
        self.categorise_total()                     #returns True or False </>= 4, respecitvely
        self.sub_prediction = ""                    #update sub prediction after K sub clustering loop
        
    def update_self(self, filter_code, *args):
        """ Update object *args with filter code """
        for arg in args:
            setattr(self, arg, filter_code)
          
    def categorise_total(self):
        """ Filter the cluster instance if it's less than 4 reads total and update filter categories"""
        if len(self.cluster_instance.all_reads) < 4:
            self.filter = True
            self.filter_code = 'TR' 
            self.prediction = 'Artefact - low reads'
            self.update_self("TR", "K1", "K1S1", "K1S2","K2","K2S1","K2S2") # update all clusters with TR filter     
 
    def categorise_k_clusters(self):
        """ Identify top level clusters and handle categorisation for classification prediction """
        self.K1_data = {k:(v[0] if isinstance(v, list) else v) for k,v in self.cluster_instance.__dict__.items()}   #Extract K1 data from instance (which object?)
        self.K2_data = {k:(v[1] if isinstance(v, list) else v) for k,v in self.cluster_instance.__dict__.items()}   #Extract K2 data from instance (which object?)
        #POTENTIAL BUG HERE - EXTRACTS ONLY THE FIRS READ FOMR ALL READS
        self.categorise_K_totals(self.K1_data, self.K2_data)         #test Kn cluster totals to remove low RC outliers updates with TRD if filtered or "" to update categories
        if self.K1 == "TRD" or self.K2 == "TRD":            #if one or both are filtered by total read count and distance
            setattr(self, "kclust_diff_cat", "NA")          #don't consider difference
            setattr(self, "kclust_SD_cat", self.categorise_kclust_SD(self.K1_data['sdev_normalised_pair'], SD2=self.K2_data['sdev_normalised_pair'])) #assign an SD category - single SD handler only the included is returned
            setattr(self, "kclust_dist_cat", self.categorise_kclust_distance(self.K1_data, self.K2_data))
        elif self.K1 != "TRD" and self.K2 != "TRD":
            setattr(self, "kclust_diff_cat", self.categorise_kclust_diff(self.cluster_instance.cluster_difference)) #categorise proportional differences in RC
            setattr(self, "kclust_SD_cat", self.categorise_kclust_SD(self.K1_data['sdev_normalised_pair'], SD2=self.K2_data['sdev_normalised_pair'], both=True)) #assign an SD category (based on all reads) can be 0          
            setattr(self, "kclust_dist_cat", self.categorise_kclust_distance(self.K1_data, self.K2_data, both=True))

    def categorise_K_totals(self, K1_data, K2_data):
        """ Remove low RC K clusters as noise if its total reads if distance > 6000; Update filter categories """
        K1_size = self.test_cluster_size(K1_data['clusters'], K1_data['ranges']) # returns false if len 1 or, 4 > x > 1 and range < 6000 """
        K2_size = self.test_cluster_size(K2_data['clusters'], K2_data['ranges'])       
        if not K1_size:                #exclude 1, <4 but > 6000
            setattr(self, "K1", "TRD") # assign total read and distance filtering criteria
        else:
            setattr(self, "K1", "")
        if not K2_size:
            setattr(self, "K2", "TRD") # assign total read and distance filtering criteria
        else:
            setattr(self, "K2", "")
        #exception handling - in the case that total is 4 and the following combinations. Overwrites previous test_cluster_size if applicable
        if len(self.K1_data['clusters']) == 2 and len(self.K2_data['clusters']) == 2:     #if 2 and 2 test each separately and then together
            setattr(self, "K1", self.handle_K_cat_distance(self.K1_data['ranges']))           #returns "" if < 6000 not 0 and not None
            setattr(self, "K2", self.handle_K_cat_distance(self.K2_data['ranges']))           #returns "" if < 6000 not 0 and not None            
        elif len(self.K1_data['clusters']) == 1 and len(self.K2_data['clusters']) == 3:   #if one and three test the three then the two together
            self.handle_3_to_1(K1_data['ranges'], self.K2_data['ranges'], "K1", "K2")                       
        elif len(self.K1_data['clusters']) == 3 and len(self.K2_data['clusters']) == 1:   #if one and three test the three then the two together
            self.handle_3_to_1(K2_data['ranges'], self.K1_data['ranges'],"K2","K1")
        elif len(self.K1_data['clusters']) == 4 and len(self.K2_data['clusters']) == 0:   #if 0 and 4, test 4 update 0
            setattr(self, "K1", self.handle_K_cat_distance(self.K1_data['ranges']))
            setattr(self, "K2", "TRD")
        elif len(K1_data['clusters']) == 0 and len(self.K2_data['clusters']) == 4:   #if 0 and 4, test 4 update 0
            setattr(self, "K2", self.handle_K_cat_distance(self.K2_data['ranges']))
            setattr(self, "K1", "TRD")
        
    def test_cluster_size(self, cluster_read_list, ranges):
        """Tests the length of a cluster, returns false if len 1 or, 4 > x > 1 and range < 6000 """
        l = len(cluster_read_list)
        if l == 1: #False if one read
            return False
        elif l < 4 and l > 1 and ranges < 6000:
            return True
        elif l < 4 and l > 1 and ranges > 6000:
            return False
        elif l > 4:
            return True
    
    def print_dict(self, dict):
        for k,v in dict.items():
            print()
            print(k,v)
    
    def handle_3_to_1(self, one, three, attr_str1, attr_str3):
        """ Handle low RC 3 and 1 cluster combination to decide whether to retain the one """
        setattr(self, attr_str3, self.handle_K_cat_distance(three))  #test the three
        print("-----")
        attr_str3 = 'self.' + attr_str3
        print(attr_str3)
        #print(self.K1['clusters'], self.K2['clusters'])
        if attr_str3 != 'TRD':                         #test together if three passes
            all_reads = self.K1_data['clusters'] + self.K2_data['clusters']
            combined = max(all_reads)-min(all_reads)  #might fail if either is None                        
            test_both = self.handle_K_cat_distance(combined)         
            if test_both == 'TRD':
                setattr(self, attr_str1, "TRD")             #update length 1 as outlier i.e. don't include in further analysis
        else:
            setattr(self, attr_str1, "TRD")                 #update length 1 as outlier if 3 fails
                
    def handle_K_cat_distance(self, ranges):
        if isinstance(ranges, int) and ranges != 0:
            if ranges >= 6000:  
                return "TRD"#not 0 or None and > 6000
            else:
                return ""   #not 0 or None and < 6000
        else:
            return "TRD"    #is 0 or None
            
    def categorise_kclust_diff(self, val):
        """ categorises the difference in proportion of total reads in each cluster """
        if val <= 20:
            return 'DLOW'   #two fairly even clusters
        elif val > 20 and val < 80:
            return 'DMED'   #mixed length clusters
        elif val >= 80:
            return 'DHIGH'  #one cluster larger than the other
            
    def categorise_kclust_SD(self, SD1, SD2, both=False):
        """ categorises the SD of the K clusters"""
        if both:                                #indicates both clusters pass TRD test and neither are therefore SD 0 
            if SD1 <= 0.01 and SD2 <= 0.01:     #likely two disparate events           
                return 'SDK1K2LOW'
            elif SD1 > 0.01 and SD2 > 0.01:     #SD1 and SD 2 > 0.01 indicates they are a single insertion event
                return  'SDK1K2HIGH'           
            elif SD1 <= 0.01 and SD2 > 0.01:    #mixed SDs see if can categorise base on other factors
                return 'SDK1LOWK2HIGH'
            elif SD1 > 0.01 and SD2 <= 0.01:    #mixed SDs -- "  " --
                return 'SDK1HIGHK2LOW'
        else:                                   #indicates on is already excluded - check for occurance of 2 and 2
            if self.K2 != "TRD" and self.K1 == "TRD":
                return self.single_sd_handler(SD2, '2') #cluster 2 included
            elif self.K2 == "TRD" and self.K1 != "TRD":
                return self.single_sd_handler(SD1, '1') #cluster 1 included
            else:
                return "SDKNA"   #both == TRD and should be excluded (unlikely but possible)
                
    def single_sd_handler(self, sd_val, Kn_in):
        """ returns string category KSD_n_LOW/HIGH """
        if sd_val <= 0.01:
            return 'SDK_' + Kn_in + '_LOW'
        else:
            return 'SDK_' + Kn_in + '_HIGH'
    
    def categorise_kclust_distance(self, K1, K2, both=False):
        """ both=True tests for Kn ranges individually and sum(Kn) to test distances for SC analysis"""
        if both == True:                        #consider ranges of both Kn if applicable - should be None
            K1r = K1['ranges']  #K1 range 
            K2r = K2['ranges']  #K2 range
            all_reads = K1['clusters'] + K2['clusters']
            Knr = max(all_reads)-min(all_reads)  #Combined ranges - can indicate number of insertions 
            if K1r < 6000 and K2r < 6000 and Knr < 6000:    #Single insertion event with no noise/randomly mapped reads? #Use main clusters as one# # COULD EVALUATE BOTH SUBS HERE
                return 'RK1K2KNLOW'   
            elif K1r < 6000 and K2r < 6000 and Knr > 6000:  #suggests there are two separate insertion events on same chromosome #Use main clusters independantly as two#
                return 'RK1K2LOWKNHIGH' 
            elif K1r > 6000 and K2r > 6000:                 #noise in both. Consider sub clusters independantly - #could be one and noise, one split into two #test for > 2 clusteras four   
                return 'RK1K2HIGH'       
            elif K1r < 6000 and K2r > 6000:                 #noise in K2, Use K1 as cluster and re-evaluate lengths of K2 sub clusters < 6000 # could be 3
                return 'RK1LOWK2HIGH'                                                
            elif K1r > 6000 and K2r < 6000:                 #noise in K2, Use K1 as cluster and re-evaluate lengths of K2 sub clusters < 6000 # could be 3 
                return 'RK1HIGHK2LOW' 
        else:                           #consider ranges of the applicable Ks - Kn should not be None
            if self.K2 != "TRD" and self.K1 == "TRD":
                K2r = K2['ranges']
                return self.single_r_handler(K2r, '2') #handle K2 single
            elif self.K2 == "TRD" and self.K1 != "TRD":
                K1r = K1['ranges']
                return self.single_r_handler(K1r, '1') #handle K1 single
            else:
                print('exception here')
                return "RKNA"        
    
    def single_r_handler(self, r_val, Kn_in):
        """ returns string category R_n_LOW/HIGH for k clusters with excluded pair"""
        if r_val < 6000:
            return 'RK_' + Kn_in + '_LOW'
        else:
            return 'RK_' + Kn_in + '_HIGH'

    def evaluate_K_cluster_distances(self):
        """ determine if can use K1 and or K2 without sub cluster analysis - separate for readability """
        #--------- clear cut cases - return True to know ready to use -------------------------
        if self.kclust_dist_cat == 'RK1K2KNLOW':    #single event, use
            self.prediction = '1EK1K2'          
            self.update_self(True, 'K1', 'K2')      #use both Kclusters clusters as ends of one event
            return True                             #return true if categorised and ready for SC analysis
        elif self.kclust_dist_cat == 'RK1K2LOWKNHIGH':
            self.prediction = '2EK1K2'
            self.update_self(True, 'K1','K2')       #Two events, use both Kclusters for separately SC analysis
            return True
        elif self.kclust_dist_cat == 'RK_1_LOW':
            self.prediction = '1EK1'
            self.update_self(True, 'K1')
            self.update_self(False, 'K2')           #Use K1 for SC analysis. K2 already excluded
            return True
        elif self.kclust_dist_cat == 'RK_2_LOW':
            self.prediction = '1EK2'              #Use K2 for SC analysis. K1 already excluded
            self.update_self(False, 'K1')
            self.update_self(True, 'K2')
            return True
        #--------- these require further analysis - return False, use prediction code dictates required analyses ---------
        elif self.kclust_dist_cat == 'RK1LOWK2HIGH':
            self.prediction = 'UK1K2sub'            #Use K1, sub analysis of K2
            self.K1 = True                          
            self.K2 = False
            return False
        elif self.kclust_dist_cat == 'RK1HIGHK2LOW':
            self.prediction = 'UK2K1sub'            #Use K2, sub analysis of K1
            self.K1 = False
            self.K2 = True
            return False
        elif self.kclust_dist_cat == 'RK1K2HIGH':
            self.prediction = 'K1K2sub'             #Sub analysis of K1 and K2
            self.update_self(False, 'K1','K2')
            return False
        elif self.kclust_dist_cat == 'RK_1_HIGH':   
            self.prediction = 'K1sub'               #Sub analysis of K1. K2 already excluded
            self.update_self(False, 'K1','K2')
            return False
        elif self.kclust_dist_cat == 'RK_2_HIGH':
            self.prediction = 'K2sub'               #Sub analysis of K2. K1 already excluded
            self.update_self(False, 'K1','K2')
            return False
        elif self.kclust_dist_cat == 'KRNA':
            self.filter = True
            return False
        else:
            print('WARNING - distance evaluation exception')
            print(self.kclust_dist_cat)
            
    def categorise_K_sub(self, other=None):     #where Kn = self.K1_data or self.K2
        """ Categorise the sub clusters of a K group. other if the other cluster is being used and need to test combined lengths
            could re-do wiht arrays of inclusion strings"""       
        if self.prediction == 'UK2K1sub':    #K2 being used already
            self.sub_test(1)                                     #categorise K1 sub class - test lengths with K2  #if single cluster identified, test lengths with K2 to determine single or multiple insertions
        elif self.prediction == 'UK1K2sub':
            self.sub_test(2)                                     #if single cluster identified, test lengths with K2 to determine single or multiple insertions
        elif self.prediction == 'K1K2sub':
            self.sub_test(1)   #subset both
            self.sub_test(2)
        elif self.prediction == 'K1sub':
            self.sub_test(1)    #K2 removed so test lengths if one Scluster is large then re-cluster that if necessary        
        elif self.prediction == 'K2sub':
            self.sub_test(2)    #K1 removed so test lengths if one Scluster is large then re-cluster that if necessary         
    
    def sub_test(self, K):
        if K == 1:
            #test sub cluster ranges
            K1_in_R = self.cluster_instance.ModZpair["c1"]["c1_in_range"]
            K1_out_R = self.cluster_instance.ModZpair["c1"]["c1_out_range"]            
            self.handle_k_sub_category(K1_in_R, K1_out_R, "K1subclust_dist_cat")
        elif K == 2:
            K2_in_R = self.cluster_instance.ModZpair["c2"]["c2_in_range"]
            K2_out_R = self.cluster_instance.ModZpair["c2"]["c2_out_range"]
            self.handle_k_sub_category(K2_in_R, K2_out_R, "K2subclust_dist_cat")
    
    def handle_k_sub_category(self, in_data, out_data, attr_str):
        """ update K1 or K2 sub cluster categories in data and out data as defined by modified z score catgorisation tests ranges are not none or 0""" 
        if isinstance(in_data, int) and in_data != 0 and isinstance(out_data, int) and out_data != 0:       #use both to update instance category
            setattr(self, attr_str, self.categorise_k_sub_distance(in_data, KnS2=out_data))                  
        elif isinstance(in_data, int) and in_data != 0 and not isinstance(out_data, int) or out_data == 0:  #use in_data only
            setattr(self, attr_str, self.categorise_k_sub_distance(in_data, sub_cluster_n=1)) 
        elif not isinstance(in_data, int) and K1_in_D == 0 and isinstance(out_data, int) or out_data != 0:  #use out_data only - unlikely
            setattr(self, attr_str, self.categorise_k_sub_distance(out_data, sub_cluster_n=2))
    
    def categorise_k_sub_distance(self, KnS1, KnS2=None, sub_cluster_n=None):
        """ both=True tests for Kn ranges individually and sum(Kn) to test distances for SC analysis
        STOPPED HERE TO OUTPUT READS WITH A SOFT CLIP IN THEM"""
        if KnS2:                           #consider ranges of both Kn if applicable - S2 is implicitly 
            S1_reads = self.cluster_instance.ModZpair["c1"]['c1_ins']
            S2_reads = self.cluster_instance.ModZpair["c2"]['c2_ins']
            all_reads = S1_reads + S2_reads
            KnSn = max(all_reads)-min(all_reads) #Combined ranges - can indicate number of insertions
            if KnS1 < 6000 and KnS2 < 6000 and KnSn < 6000:   #should be impossible Kn would be used already
                return 'RS1S2SNLOW '   
            elif KnS1 < 6000 and KnS2 < 6000 and KnSn > 6000: #suggests there are two insertion events in this k_cluster. Use subclusters independantly
                return 'RS1S2SNHIGH' 
            elif KnS1 > 6000 and KnS2 > 6000:                 #noise in both. Consider sub clusters independantly - #could be one and noise, one split into two #test for > 2 clusteras four
                #test counts of high clusters - remove if <= 4
                S1_count = test_count(S1_reads, KnS1)
                S2_count = test_count(S2_reads, KnS2)
                if not S1_count and not S2_count:
                    return #immediately discard this cub cluster 
                #return 'RS1S2HIGH'       
            elif KnS1 < 6000 and KnS2 > 6000:                 #noise in S2, Use S1 as cluster check lengths of S2 and re-clsuter if required
                #test counts of high clusters - remove if <= 4
                S2_count = test_count(S2_reads, KnS2)
                if not S2_count:
                    return   #immediately discard code as too low
                else:
                    if S2_count == 'RC':
                        return #code recluster S2 because lots of reads
                    elif S2_count == 'Highrange':
                        return    #use cluster but with a high range code to show it >= 6000kb <25kb
                        #return 'RS1LOWS2HIGH' 'RS1LOWRKS2'                                                
            elif KnS1 > 6000 and KnS2 < 6000:                 #noise in S1, Use S2 as cluster check lengths of S1 and re-cluster if required
                #test counts of high clusters - remove if <= 4
                S1_count = test_count(S1_reads, KnS1)
                if not S1_count:
                    return   #immediately discard code as too low
                else:
                    if S1_count == 'RC':
                        return #code recluster S2 because lots of reads
                    elif S1_count == 'Highrange':
                        return    #use cluster but with a high range code to show it >= 6000kb <25k
                #return 'RS1HIGHS2LOW' 
            else:
                print('exception categorise_k_sub_distance')
        else:                                                 #only one sub cluster as other is 0 or none
            return self.single_sub_r_handler(KnS1, sub_cluster_n)  #KnS1 may be S2 so sub_cluster_n is passed to confirm

    def single_sub_r_handler(self, r_val, sub_cluster_n):
        """ returns string category R_n_LOW/HIGH for k clusters with excluded pair"""
        if r_val < 6000:
            return 'RS_' + str(sub_cluster_n) + '_LOW'
        else:
            return 'RS_' + str(sub_cluster_n) + '_HIGH'

  #  def test_count(read_list, range):
  #      """ returns false if <= 4 reads (snl) and > 6000kb sn(r) if > 4 reads and < 25kb - use sub"""    
  #      if len(read_list) <= 4:
  #          return False #immediately discard from re_clutering
  #      else:
  #          if max(read_list)-min(read _list) < 25000:  #test lenght - some seem to be at similar sites resulting in wide clusters (21KB is largest I've seen)
  #              return 'Highrange'
  #          else:
  #              return 'RC'
    
    
    
    
    
    
    
    