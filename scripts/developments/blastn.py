### can be used for pairwise blasting of insertion sites form non-model organism/de novo reference (e.g. Ustilago AB33 SC read seqs against 521 genome)
# pairwise parser requried becasue doesn't always return hits

#python blastn.py -q /gpfs/ts0/projects/Research_Project-184686/resources/references/ustilago_maydis/ensembl/ble_cassette.fa -s /gpfs/ts0/projects/Research_Project-184686/resources/references/ustilago_maydis/ensembl/Ustilago_maydis.Umaydis521_2.0.dna.toplevel.hygp2343.fa -o test_blast

#reference must be the subject

import subprocess, argparse
from Bio.Blast import NCBIXML

def pairwise_blast(subject_file, query_file):
    """ blastn two nucleotide sequences - output format must be 0 """
    cmd = ['blastn', '-query', query_file, '-subject', subject_file, '-outfmt', '0']
    ps = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    blast_text = ps.communicate()[0].decode()
    return blast_text                                                                   #decode from byte string
       
def parse_pairwise_blast(input_text):
    """ Returns only matching alignments between two sequences"""     
    input_text = input_text.split('\n')                 #remove first three as not needed
    alignment_count = 0                                 #used as keys to alignment dict
    hit_bool = False                                    #switch to determine when it is and isnt an active hit
    alignment_dict = {"blast_version" : input_text[0]}
    for idx,line in enumerate(input_text):              #iterate through text array with indexes        
        if 'Score' in line and hit_bool == False:       #then its the first 
            alignment_count += 1
            hit_bool = True                             #flip the hit switch
            alignment_dict[alignment_count] = input_text[idx-6:idx +1] #store that index - 7 to get the start of the alignment #need to add 1 to index as 0 based
            continue #continue to next iteration
        if hit_bool == True:
            if "Effective search space used:" not in line:   #search for the end of the alignment  
                alignment_dict[alignment_count].append(line) #add to dict if not end of hit
            else:
                alignment_dict[alignment_count].append(line) #add this one any way
                hit_bool = False #reset hit switch as hit is completed
    return alignment_dict

def pairwise_blast_to_bed(subject_file, query_file, outpref):
    """ output bed file of blast matches """
    cmd = ['blastn', '-query', query_file, '-subject', subject_file, '-outfmt', '6 qseqid qstart qend sseqid sstart send frames']
    ps = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    blast_text = ps.communicate()[0].decode().split('\n')
    print(blast_text)
    outf = outpref + '.bed'
    chrom_list = chrom_from_subject(subject_file) #lookup chrom from subject/reference 'grep all '>'
    with open(outf, 'w') as ouf:
        for match in blast_text:
            if match != '':
                match = match.split('\t')
                print(match)
                chrom_index = int(match[3].split('_')[-1]) - 1 #lookup by index e.g. subject_n -1 e.g. Subject_10 is the 10th one/9th index
                ouf.write(chrom_list[chrom_index] + '\t' + match[4] + '\t' + match[5] + '\t.\t.\t' + '+' + '\n') #chrom, start, stop forward
                ouf.write(chrom_list[chrom_index] + '\t' + match[4] + '\t' + match[5] + '\t.\t.\t' + '-' + '\n') #chrom, start, stop reverse
    return outf

def chrom_from_subject(subject_file):
    """ Subject chromosomes are counted 1 - n not what the output is e.g. Subject_1056 could be contig 1000 if there are 56 scaffolds"""
    ps = subprocess.Popen(['grep', '>', subject_file], stdout=subprocess.PIPE)
    chroms = ps.communicate()[0].decode().split('\n')
    return [chrom.split(' ')[0].strip('>') for chrom in chroms] #split and strip to retain chorm number only

def bed_closest_gff(alignment_bed, gff_521):
    """ first looks for direct intersects, if there are none looks for the closest which is  """
    #the bed file of intersects outputs two entries for each intersect. One for forward and one for reverse. 
    #this is becase the aim is to see what is closest to this and will be done for each strand
    #bed tools closest gff report distance wrt reference i.e. lower coordinates are upstream. small s for same strand, 
    #ignores upstream of intersect (if gff coords are lower they will be ignored)
    #bedtools closest -a alignment_bed -b gff_521 -s -D ref -iu
    #bedtools closest -a test_blast.bed -b test_3.gff -s -D ref -iu tested with this
    
def output_alingment_dict(alignment_dict, outfile):
    """ Need to supply output file and write the lines to it rather than printing """
    with open(outfile, 'w') as ouf:
        ouf.write(alignment_dict['blast_version'] + '\n')
        for k,v in alignment_dict.items():
            if k != 'blast_version':
                for i in v:
                    ouf.write(i + '\n')
 

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument('-q', '--query', help='blastn query fasta file')
    p.add_argument('-s', '--subject', help='blastn subject fasta file')
    p.add_argument('-o', '--outfile', help='output_file_prefix')
    p.add_argument('-db', '--database', help='blastn against specified dtabase')
    args = p.parse_args()
    if args.database is not None:
        pass #run against nucluotide 
    else:
        pairwise_blast = pairwise_blast(args.subject, args.query)   #perform pairwise blast
        alignment_dict = parse_pairwise_blast(pairwise_blast)       #dictionary of alignments
        output_alingment_dict(alignment_dict, args.outfile)         #output alignemnts to file
        pairwise_blast_to_bed(args.subject, args.query, args.outfile)             #repeats blast to generate bed file 
        #
        
        
"""
  

def database_blast():
    pass
    
def parse_db_blast(input_xml):    
    blast_parser = NCBIStandalone.BlastParser()
    blast_record = blast_parser.parse(open(input_text))
    E_VALUE_THRESH = 0.04
    for alignment in blast_record.alignments:
        for hsp in alignment.hsps:
            if hsp.expect < E_VALUE_THRESH:
                print('****Alignment****')
                print('sequence:', alignment.title)
                print('length:', alignment.length)
                print('e value:', hsp.expect)
                print(hsp.query[0:75] + '...')
                print(hsp.match[0:75] + '...')
                print(hsp.sbjct[0:75] + '...')
    

 Options 6, 7, and 10 can be additionally configured to produce
   a custom format specified by space delimited format specifiers.
   The supported format specifiers are:
            qseqid means Query Seq-id
               qgi means Query GI
              qacc means Query accesion
           qaccver means Query accesion.version
            sseqid means Subject Seq-id
         sallseqid means All subject Seq-id(s), separated by a ';'
               sgi means Subject GI
            sallgi means All subject GIs
              sacc means Subject accession
           saccver means Subject accession.version
           sallacc means All subject accessions
            qstart means Start of alignment in query
              qend means End of alignment in query
            sstart means Start of alignment in subject
              send means End of alignment in subject
              qseq means Aligned part of query sequence
              sseq means Aligned part of subject sequence
            evalue means Expect value
          bitscore means Bit score
             score means Raw score
            length means Alignment length
            pident means Percentage of identical matches
            nident means Number of identical matches
          mismatch means Number of mismatches
          positive means Number of positive-scoring matches
           gapopen means Number of gap openings
              gaps means Total number of gaps
              ppos means Percentage of positive-scoring matches
            frames means Query and subject frames separated by a '/'
            qframe means Query frame
            sframe means Subject frame
              btop means Blast traceback operations (BTOP)
   When not provided, the default value is:
   'qseqid sseqid pident length mismatch gapopen qstart qend sstart send
   evalue bitscore', which is equivalent to the keyword 'std'
        
"""