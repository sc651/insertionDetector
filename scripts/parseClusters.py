from sklearn.cluster import KMeans
import numpy as np
from scripts.ClusterPair import *
import os

def kClusters(KI_Obj, cluster_level, parent_cluster=None):
    """ Generate k-means clusters for each chromosome (k=2) and intialise ClusterPair object """
    cluster_pairs  = []
    
    # Check distance of raw reads to see if clustering is necessary in the first instance
    if not parent_cluster:  # If parent cluster, use separate loop to extract parent custer reads whcih need a new cluster
        for chrom, reads in KI_Obj.rePairedInterRegionReads.items():                        # Iterate through repaired reads
            start_poss = sorted([pair.inter.reference_start for pair in reads], key=int)    # List of read start positions sorted numerically
            fit_data = KMeans(n_clusters=2).fit(np.asarray(start_poss).reshape(-1,1))       # Number of k clusters, reshape data for clustering array of arrays, fit to 2 clusters
            cluster_pairs.append(ClusterPair(chrom, [[x for i,x in enumerate(start_poss) if fit_data.labels_[i] == Kn] for Kn in range(2)], cluster_level)) # Hard coded two clusters here Kn = cluster number
    #elif parent cluster do re-clustering (already been checked)
    return cluster_pairs
    
def sub_to_file(ouf, clusterPair, this_cluster, sub_cluster, inlier=False):
    if inlier:
        z_var = 'inlier'
    else:
        z_var = 'outlier'
    KnZn_out = ['', sub_cluster, '', \
                getattr(getattr(clusterPair, this_cluster+'_modz'), z_var + '_count'), \
                getattr(getattr(clusterPair, this_cluster+'_modz'), z_var + '_range'), \
                str(getattr(getattr(clusterPair, this_cluster+'_modz'), z_var + 's'))]
    ouf.write('{0}'.format('\t'.join(KnZn_out)) +'\n')                
        
def writeClusters(KI_obj, level=1):
    """ Write cluster info to text for manual curation """
    headers = ['chr', 'cluster_category', 'chrom_total_read_pairs', 'cluster_total_reads', 'cluster_range', 'read_start_positions'] 
    outdir = os.getcwd() + '/outputs'
    if not os.path.exists(outdir + '/clustering/' + KI_obj.pool):
        os.makedirs(outdir + '/clustering/' + KI_obj.pool)    #make output directory for each pool
    with open('outputs/clustering/' + KI_obj.pool + '/' + KI_obj.pool + '_clusters' + str(level) + '.txt', 'w') as ouf:
        ouf.write('{0}'.format('\t'.join(headers) + '\n'))  #write headers to file
        for clusterPair in KI_obj.cluster_levels:
            for i in range(2):          # Need to iterate through both K clusters
                this_cluster = 'K' + str(i+1)
                chrom_total_pairs = str(len(KI_obj.rePairedInterRegionReads[clusterPair.chrom]))                    
                Kn_out = [clusterPair.chrom, this_cluster, chrom_total_pairs, \
                               str(len(getattr(clusterPair, this_cluster+'_modz').cluster)), \
                               str(getattr(clusterPair, this_cluster+'_modz').cluster_range), \
                               str(getattr(clusterPair, this_cluster+'_modz').cluster)]
                ouf.write('{0}'.format('\t'.join(Kn_out)) +'\n')
                sub_to_file(ouf, clusterPair, this_cluster, this_cluster + 'z1', inlier=True)
                sub_to_file(ouf, clusterPair, this_cluster, this_cluster + 'z2')
            ouf.write('\n')
                
      
         