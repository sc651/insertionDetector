import pysam, glob, os, subprocess
import pandas as pd
import numpy as np
# Script to:
    # Parse clipped reads from the KI region
    # Separate hard clipped and soft clipped 
    # Retrieve original un-hard clipped reads from fastq files

def isolate_clips(KI_obj):
    """ Take known insertion object re-paired inter region reads 
        ouptut SC and HC fastq files realignment """
    cwd = os.getcwd()
    realigned_md_bam = cwd + '/outputs/re_alignment/' + KI_obj.pool + '/' + KI_obj.pool + '_clip_region_rln.md.bam'
    if os.path.isfile(realigned_md_bam):    # If pool has already been realigned, don't do it again. Maybe remove this after testing complete.
        return "na", "na"
    else:    
        message('Using region reads identified as having interchromosomal pair for clipped read selection', KI_obj.interRegionReads)
        hard_clips, soft_clips = isolate_clipped_reads(KI_obj.pairedRegionReads) # (KI_obj.interRegionReads)    #USE PAIRED REGION READS? # Select hard clipped and soft clipped reads from KI region reads with inter chromosomal pair
        hard_clip_fq = retrieve_original_hard_clipped(cwd, hard_clips, KI_obj.pool, KI_obj.fq1, KI_obj.fq2)   # Look up original pair of any hard clipped read for realignment
        soft_clip_bam = soft_clipped_to_bam(cwd, soft_clips, KI_obj.bam, KI_obj.pool)    # Write soft clipped read objects to bam file 
        soft_clip_fq = bam_to_fq(soft_clip_bam)     # 
        return hard_clip_fq, soft_clip_fq           # Ouput clipped reads to bam for realignment
    
def isolate_clipped_reads(read_list):
    """ Takes a pysam read list and isolates those which are clipped """
    soft_clipped_reads = []     # List for soft clipped reads
    hard_clipped_reads = []     # List for hard clipped reads
    # CHECK FOR DUPLICATE READS IN THESE LISTS 
    for read_object in read_list:   # Iterate through provided read list    
        try:
            cigar_type, quantity = zip(*read_object.cigartuples) # Zip cigar tuples into a list easily identify clipped reads
            if 4 in cigar_type and 5 in cigar_type:
                hard_clipped_reads.append(read_object)  # Update hard clipped reads if the reads are hard and soft clipped
            elif 5 in cigar_type:
                hard_clipped_reads.append(read_object)  # Update hard clipped reads with no soft clips
            elif 4 in cigar_type:
                soft_clipped_reads.append(read_object)  # Update soft clipped reads with no hard clips
        except:
            print('Read has no cigar' + str(read_object))
    message('Reads have been soft clipped in alignment', soft_clipped_reads)
    message('Reads have been hard clipped in alignment', hard_clipped_reads)
    return hard_clipped_reads, soft_clipped_reads
    
def retrieve_original_hard_clipped(cwd, hard_clips, pool, fq1, fq2):
    """ Create a list of read ids to use with seqtk subseq. Return a fastq of hard clipped reads (effectively single ended) """
    lst = cwd + '/outputs/re_alignment/' + pool + '/' + pool + '_hard_clips.list'
    hard_clipped_fq = cwd + '/outputs/re_alignment/' + pool + '/' + pool + '_hard_clips.fq'
    if os.path.exists(lst):
        subprocess.call(['rm', lst])     # Delete this if it exists because it's appended to - re-running with the file present would create rrrounous results
    else:
        if not os.path.exists(cwd + '/outputs/re_alignment/' + pool):   # If pool file doesn't exist 
            os.makedirs(cwd + '/outputs/re_alignment/' + pool)          # Make it - necessary if it wa re run and the deleted 
    with open(lst, 'w') as id_list:
        for read in hard_clips:
            id_list.write(read.query_name + '\n')
    if os.path.exists(hard_clipped_fq):
        subprocess.call(['rm', hard_clipped_fq]) # Delete this if it exists because it's appended to - re-running with the file present would duplicate data
    with open(hard_clipped_fq, 'a') as fq:
        print('Retrieving hard clipped reads from fq1')
        subprocess.call(['/gpfs/ts0/projects/Research_Project-184686/software/seqtk/seqtk', 'subseq', fq1, lst], stdout=fq) # Extract listed reads from fq1
        print('Retrieving hard clipped reads from fq2')
        subprocess.call(['/gpfs/ts0/projects/Research_Project-184686/software/seqtk/seqtk', 'subseq', fq2, lst], stdout=fq) # Extract listed reads from fq1
    os.remove(lst)
    return hard_clipped_fq

def soft_clipped_to_bam(cwd, read_list, template_bam, pool):
    """ Output KI clipped reads to bam by pool """
    template_bam = pysam.AlignmentFile(template_bam, 'rb')
    outdir = cwd + '/outputs'
    out_bam_path = outdir + '/re_alignment/' + pool + '/' + pool + '_clip_region.bam'
    out_bam = pysam.AlignmentFile(out_bam_path, 'wb', template=template_bam)
    for read in read_list:
        out_bam.write(read)
    out_bam.close()
    return out_bam_path
    
def bam_to_fq(soft_clip_bam): #, hard_clip_fq):
    """ Converts sc reads to bam file and appends hard clipped read from fq1 and fq2 """
    bam_root, ext = os.path.splitext(soft_clip_bam)
    soft_clip_fq = bam_root + '.fq'
    if os.path.exists(soft_clip_fq):              # Need to remove if being re-run because to catch error??
        subprocess.call(['rm', soft_clip_fq])
    subprocess.call(['bedtools', 'bamtofastq', '-i', soft_clip_bam, '-fq', soft_clip_fq])
    os.remove(soft_clip_bam)    # Remove intemediate bam file
    return soft_clip_fq
    
def message(message, lst=None):
    """ Prints length of list and message string """
    if lst:
        print(str(len(lst)) + ' ' + message)
    else:
        print(message)
           