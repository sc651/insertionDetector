import subprocess, time, os

class Intersect_obj():
 
    def __init__(self, intersect):
        """ One of these for each object -wb return intersect """
        self.parse_intersect(intersect)

    def parse_intersect(self, intersect):
        spl = intersect.split('\t')                             
        self.intersect_chrom = spl[0]                           # region chrom
        self.intersect_start = spl[1]                           # starts intersecting with region here 
        self.intersect_end = spl[2]                             # stops intersecting with region here
        self.annotator = spl[4]                                 # annotating body --- rest are from gff portion of intersect
        self.feat_type = spl[5]                                 # type of gff feature
        self.feat_start = spl[6]                                # feature start
        self.feat_end = spl[7]                                  # feature end
        self.feat_strand = spl[9]                               # feature strand
        self.info_dict = self.parse_info_to_dict(spl[11])       # parse gff entry into dict
        self.seq_info = ""
        self.seq = ""
        
    def parse_info_to_dict(self, info):
        spl = (info.split(';'))
        info_dict = {}
        for info in spl:
            spl_2 = info.split('=')
            info_dict[spl_2[0]] = spl_2[1]        
        return info_dict

class Insertion():
   
    region = ""            # Coordinates +/- 5Kb of insertion min and max
    region_seq = ""        # DNA seq of region
    min_clip_seq = ""      # 
    max_clip_seq = ""      # 
   
    def __init__(self, id, chrom, min_clip, max_clip, range, count, pool):
        self.id = id                #Insertion id 
        self.chrom = chrom          # insertion chromosome
        self.min_clip = min_clip    # Insertion minimun clip location
        self.max_clip = max_clip    # Insertion maximum clip location
        self.range = range          # range between soft clip locations
        self.count = count          # Number of soft clip reads
        self.pool = pool            # The pool the insertion originated from
        if min_clip == max_clip:
            self.single_end = True
        else:
            self.single_end = False
        self.intersecting_features = []          # List of gene feature objects from intersect with gff file
        
def run_nucleotide_extraction(args):
    """ Take all input arguments """
    ins_objs = generate_insertion_objects(args.insertions)
    for i in ins_objs:
        print(i.__dict__)
    for ins_obj in ins_objs:
        region_extraction(ins_obj, args)
        obtain_region_features(ins_obj, args)
        feature_extraction(ins_obj, args)
        output_region_data(ins_obj)

def generate_insertion_objects(insertion_file):
    insertion_object_list = []
    with open(insertion_file, 'r') as inf: # Iterate insertion file
        for line in inf:
            spl = line.strip('\n').split('\t')
            print(spl)
            insertion_object_list.append(Insertion(spl[0], spl[1], spl[2], spl[3], spl[4], spl[5], spl[6]))   #generate object for each line
    return insertion_object_list # return list of insertion objects   
    
def region_extraction(ins_obj, args):
    """ Extract nucleotides +/- insertion sites """
    seqtk = "/gpfs/ts0/projects/Research_Project-184686/software/seqtk/seqtk"
    ins_obj.region = ins_obj.chrom + ':' + str(int(ins_obj.min_clip) - 5000) + '-' + str(int(ins_obj.max_clip) + 5000) # Update region
    bed_f = os.getcwd() + '/' + ins_obj.id + '_temp.bed'
    with open(bed_f, 'w') as bed:
        bed.write(ins_obj.chrom + '\t' + str(int(ins_obj.min_clip) - 5000) + '\t' + str(int(ins_obj.max_clip) + 5000) + '\n')  # Write region
        bed.write(ins_obj.chrom + '\t' + str(int(ins_obj.min_clip) - 101) + '\t' + str(int(ins_obj.min_clip)) + '\n')      # Lower clip
        bed.write(ins_obj.chrom + '\t' + str(int(ins_obj.max_clip) -1) + '\t' + str(int(ins_obj.max_clip) + 100) + '\n')       # Upper clip
    seq_data = seq_from_fasta(seqtk, bed_f, args.reference)
    ins_obj.region_seq = seq_data[0][-1]
    ins_obj.min_clip_seq = seq_data[1][-1]
    ins_obj.max_clip_seq = seq_data[2][-1]
    os.remove(bed_f)
    
def feature_extraction(ins_obj, args):
    """ Update each feature with seq """
    seqtk = "/gpfs/ts0/projects/Research_Project-184686/software/seqtk/seqtk"
    for feat in ins_obj.intersecting_features:
        bed_f = os.getcwd() + '/' + ins_obj.id + '_' + feat.info_dict['gene_id'] + '_tempfeat.bed'
        with open(bed_f, 'w') as bed:
            bed.write(feat.intersect_chrom + '\t' + feat.intersect_start + '\t' + feat.intersect_end)
        seq_data = seq_from_fasta(seqtk, bed_f, args.reference)
        seq_data = [seq for seq in seq_data if seq != ['']]
        feat.seq_info = seq_data[0][0]
        feat.seq = seq_data[0][-1]
        os.remove(bed_f)
    
def seq_from_fasta(seqtk, region_bed, fa):
    """ Return sequences from fasta file in list of lists """
    time.sleep(0.05)                                        # Add delay for I/O
    cmd = [seqtk,'subseq', fa, region_bed]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    data = str(p.communicate()[0], 'utf-8').split('\n')
    return chunks(data, 2)

def chunks(l, n):
    """ split list l into chunks of n """
    chunk_list = []
    for i in range(0, len(l), n):
        chunk_list.append(l[i:i+n])
    return chunk_list    

def obtain_region_features(ins_obj, args):
    """ Get intersecting features from region """
    this_bed = ins_obj.id + '_region.bed'
    with open(this_bed, 'w') as ouf:
        spl =  ins_obj.region.split(':')[-1].split('-') # Split chrom:lower-upper into [lower, upper]
        region_data = [ins_obj.chrom, spl[0], spl[1]]
        ouf.write('{0}'.format('\t'.join(region_data)))
    cmd = ['bedtools', 'intersect', '-a', this_bed, '-b', args.gff, '-wb']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    data = str(p.communicate()[0], 'utf-8').split('\n')
    for intersect in [i for i in data if i]: # Iterate through intersects returned from bedtools [removes empty strings from intersect list]
        ins_obj.intersecting_features.append(Intersect_obj(intersect))
    os.remove(this_bed)
    return ins_obj
    
def output_region_data(ins_obj):
    """ Output data for each region """
    if not os.path.exists(os.getcwd() + '/outputs/borders/'):
        os.mkdir(os.getcwd() + '/outputs/borders/')
    output = os.getcwd() + '/outputs/borders/' + ins_obj.id + '_borders'
    with open(output, 'w') as ouf:
        headers = ['insertion_id','chrom','min_clip_pos','max_clip_pos','clip_range','clip_count','pool', 'single_ended']
        region = [ins_obj.id, ins_obj.chrom, ins_obj.min_clip, ins_obj.max_clip, ins_obj.range, ins_obj.count, ins_obj.pool, str(ins_obj.single_end)]
        region_seq = ['region', ins_obj.region_seq]
        lower_clip = ['lower_clip', ins_obj.min_clip_seq]
        upper_clip = ['upper_clip', ins_obj.max_clip_seq]
        ouf.write('{0}'.format('\t'.join(headers) + '\n'))
        ouf.write('{0}'.format('\t'.join(region) + '\n\n'))
        ouf.write('{0}'.format('\t'.join(region_seq) + '\n\n'))
        ouf.write('{0}'.format('\t'.join(lower_clip) + '\n\n'))
        ouf.write('{0}'.format('\t'.join(upper_clip) + '\n\n' + '###### Intersecting gene sequences #######' + '\n\n'))
        feature_headers = ['Gene_id', 'type', 'feature_start', 'feature_end, strand', 'intersect_start', 'intersect_end']
        ouf.write('{0}'.format('\t'.join(feature_headers) + '\n'))
        print('\n', ins_obj.id)
        for feat in ins_obj.intersecting_features:
            feature_info = [feat.info_dict['gene_id'], feat.feat_type, feat.feat_start, feat.feat_end, feat.feat_strand, feat.intersect_start, feat.intersect_end]
            ouf.write('{0}'.format('\t'.join(feature_info) + '\n' + feat.seq + '\n\n'))

            #print(feat.feat_type, feat.feat)
            #print(feat.intersect_chrom, feat.intersect_start, feat.intersect_end)
            #print(int(feat.intersect_end) - int(feat.intersect_start))
            #print(len(feat.seq))
            #print(feat.seq_info)
            


    
