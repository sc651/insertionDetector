# Retrieves amino acid sequences from known genes and updated annotation file
# Updates annotation dataframe 

import requests, sys, os, time
import pandas as pd

def obtain_peptides(KI_obj):
    """ Gets protein sequences and updates annotation dataframe """
    annotation_df = pd.read_csv(KI_obj.annotated_bed, sep='\t') # Read annotated bed file to df
    feature_dict = split_feature_annotation(annotation_df)      # Turn features into dictionary for each row
    df = pd.DataFrame.from_dict(feature_dict, orient='index')   # Turn feature dict into df
    annotation_df = pd.merge(annotation_df, df, left_index=True, right_index=True)      # Merge annotation df and feature df
    gene_set = extract_gene_from_feature_annotation(annotation_df['feat_annotation'])   # Extract unique genes from df
    annotation_df['protein_acc'] = ""                                                   # Add fields to datafram for updating uniprot accession ID
    annotation_df['protein_fa'] = ""                                                    # Uniprot protein sequence (fasta)
    annotation_df['protein_fa_header'] = ""                                             # Fasta header '>...'
    gene_dict = retrieve_protein_fa(gene_set)                                           # Lookup protein sequences for each gene                                                    
    updated_df = update_annotation_df(annotation_df, gene_dict)                         # Parse protein seq outputs
    return updated_df
    
def split_feature_annotation(annotation_df):
    """ Identify all the possible annotation headers """
    feature_series = annotation_df['feat_annotation']   # Extract feature series
    this_dict = {}
    for idx, annotation in feature_series.iteritems():  # create { idx : { feature_header : feture_details }, idx }    
        remove_ID = annotation.split(':', 1)[1]         # Remove data from before the annotation proper e.g. ID=gene:UMAG_10668;biotype=protein_coding;description=hypothetical protein;gene_id=UMAG_10668;logic_name=ena
        this_dict[idx] = dict(feature.split('=') for feature in remove_ID.split(';')[1:])  # Append the feature dictionary to the index it came from 
    return this_dict
    
def update_annotation_df(annotation_df, gene_dict):
    for gene, gene_data_dict in gene_dict.items():  # Iterate over gene dict to add fasta seqs to df 
        if gene_data_dict:                          # Iterate over each accession
            if len(gene_data_dict.keys()) > 1:      # Hihglight if there are >1 so can build in exception
                print('warning -- include multiple transcript handler')
            else:
                acc = list(gene_data_dict.keys())[0]    # Currently only one key whihc is accession   
                annotation_df.loc[annotation_df['feat_annotation'].str.contains(gene), 'protein_acc'] = acc # Update accession if row annotations contain the gene
                split_fasta = list(gene_data_dict.values())[0][0].split('\n')                               # Get first fasta if there are multiple. Currently only one value i.d the fasta.   
                annotation_df.loc[annotation_df['feat_annotation'].str.contains(gene), 'protein_fa_header'] = split_fasta[0]    # Get header from first index of split fasta by '\n'
                annotation_df.loc[annotation_df['feat_annotation'].str.contains(gene), 'protein_fa'] = "".join(split_fasta[1:]) # Join up the remaining lines of the fasta into a single string
    return annotation_df
                    
def retrieve_protein_fa(gene_set):
    """ Get protein seqs for each gene, return results in dictionary  """
    print('Retrieving peptide seqs')
    gene_dict = {}                                  # To update gene with peptide fastas {gene : {uniprot_acc : [fasta1, fastan]}
    for gene in gene_set:                           # Iterate through unique genes
        gene_dict[gene] = {}                        # Add each gene as a key and add a new dict to be updated with gene specific information
        protein_id = ebi_search(gene, db_accession=True)         # Get the protein id/acc from ebi api
        if protein_id:
            fasta = get_uniprot_fasta(protein_id)   # Get the peptide fasta seq from uniprot
            gene_dict[gene][protein_id] = [fasta]   # Update gene_dict with retrieved data and protein_Id as the only key currently with the fasta
        else:
            print('no_protein id or gene or two isoforms for ' + gene)
    return gene_dict 
    
def extract_gene_from_feature_annotation(annotation_series):
    """ Parse a gff annotation to return unique genes id """
    gene_ids = []                                                               # List to be appended with gene ids in the annotated bed file
    for ann in annotation_series:                                               # Iterate through the 'feature annotation' series
        gene_id = [i.split('=')[-1] for i in ann.split(';') if 'gene_id=' in i] # Extract the gene id from the feature annotation - requries gff to be annotated with 'gene_id='
        if len(gene_id) == 0:                                                   # Output waring if no gene identified 
            print('WARNING ---- no gene id extracted for anotation ', ann)      
        gene_ids.append(gene_id)                                                # Append gene ids to list
    return set([gene_id for gene_id_list in gene_ids for gene_id in gene_id_list]) # Flatten list of gene id lists and set so no duplicates
      
def ebi_search(gene_name, protein_search=False, db_accession=False):
    """ Retrieves protein sequences from EBI rest API """
    time.sleep(0.1)
    ebi_server = 'http://www.ebi.ac.uk/proteins/api/'               # server remains same regardless
    if db_accession:
        ebi_ext = ebi_server + "proteins/EnsemblFungi:" + gene_name + "?offset=0&size=100"
    elif protein_search:
        ebi_ext = ebi_server + 'proteins?offset=0&size=100&gene=' + gene_name   # Request extension for lookup by gene id
    r = requests.get(ebi_ext, headers={ "Accept" : "application/json" })    # Get request
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    return parse_uniprot_accession(r.json())                                # Pass response to id/accession parser. Handles multiple accessions
    
def parse_uniprot_accession(ebi_json):
    """ parse output to return uniprot accession code  """
    if len(ebi_json) > 1:
        print('ebi not single entry. Expected single entry in response')
    else:
        for i in ebi_json:
            return i['accession'] # Return the id/accession
        
def get_uniprot_fasta(uniprot_id):
    time.sleep(0.1)
    uniprot_server = "https://www.uniprot.org/uniprot/"                     # Uniprote API base   
    extension = uniprot_server + uniprot_id + '.fasta'                      # uniprot extension e.g. P12345.fasta 
    r = requests.get(extension, headers={"Accept" : "application/json"})    # Make request 
    if not r.ok:
        r.raise_for_status()
        sys.exit()
    return r.text           # Return the fasta text response

def ouput_filtered_peptides(KI_obj):
    """ Output peptide sequences from annotated and filtered dataframe """
    cwd = os.getcwd()
    if not os.path.exists(cwd + '/outputs/insertions/' + KI_obj.pool):
        os.mkdir(cwd + '/outputs/insertions/' + KI_obj.pool)
    pep_file = cwd + '/outputs/insertions/' + KI_obj.pool +'/' + KI_obj.pool + '_protein_seqs.fa'
    with open(pep_file, 'w') as pep:
        for idx, row in KI_obj.annotation_df.iterrows():
            pep.write(row['protein_fa_header'] + row['gene_id'] + '\n')
            pep.write(row['protein_fa'] + '\n\n')
    return pep_file















