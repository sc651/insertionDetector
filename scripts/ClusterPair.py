from scripts.Modz import *

class ClusterPair():
    """ Defines k-means cluster pair, applies zscore modification and filter
        to remove k-cluster anomolies
    """    
    def __init__(self, chrom, cluster_pair, cluster_level, clustered=True, parent_cluster=None):
        ''' Iitialises Modz on k cluster '''
        self.parent_cluster = parent_cluster    # This is ClusterPair_object + K1/K2 for head clusters. Otherwise its ModZobject + inliers/outliers
        self.level = cluster_level
        self.chrom = chrom
        if not clustered:                       # This is relevant in the first instance as subsequent iterations would be checked
            self.K0 =  cluster_pair             # cluster_pair here is all of the read positions of unclustered array 
        else:
            self.K1_modz = ModZ(cluster_pair[0])    # K cluster 1 of 2 with modz inliers and outliers
            self.K2_modz = ModZ(cluster_pair[1])    # K cluster 2 of 2 with modz inliers and outliers
        