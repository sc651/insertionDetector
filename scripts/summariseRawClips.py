import pysam, os
import pandas as pd
import numpy as np

def output_unannotated_bed(KI_obj):
    """ Output the minimum and maximum insertion points per chromosome and intersect the gff """
    clip_df = pd.read_csv(KI_obj.realigned_raw_reads, sep='\t') #read the realigned.output_sc_reads into data frame
    df_chrom_list = [] # realigned sc reads output by chromosome
    raw_root, ext = os.path.splitext(KI_obj.realigned_raw_reads)
    bed_f = raw_root + '.bed'
    df_chrom_list = [clip_df[clip_df['chrom'] == chrom] for chrom in clip_df.chrom.unique()]  #sort into sub dataframes by chromosome
    with open(bed_f, 'w') as bed_file, open(raw_root + '.unadjusted.bed', 'w') as raw_bed:
        for df_chrom in df_chrom_list:
            chrom = df_chrom['chrom'].iloc[0] # Get chromosome from first row
            df_chrom = df_chrom[(df_chrom['clip_location'] != '-') & (df_chrom['clip_location'] != '--') & (df_chrom['clip_location'] != 'None')] # Separate by chrom, exclude null data
            if not df_chrom.empty:
                maxi = df_chrom.clip_location.astype(int).max() # Highest location 
                mini = df_chrom.clip_location.astype(int).min() # Lowest location
                raw_bed.write('{0}'.format('\t'.join([str(chrom), str(mini), str(maxi), str(maxi - mini), str(df_chrom.clip_location.count())])) +'\n') # Output raw, ungrouped bed file 
                if maxi - mini > 2000:
                    grouped_lists = group_by_average_distance(df_chrom.clip_location.astype(int).tolist())  # Group sc locations by average                           
                    for sc_locations in combine_grouped_lists(grouped_lists):  # Iterate through potential soft clipped locations re-combined by distance
                        maxi = max(sc_locations)        # Maximum position from this grouping
                        mini = min(sc_locations)        # Minimum position form this grouping
                        bed_file.write('{0}'.format('\t'.join([str(chrom), str(mini), str(maxi), str(maxi - mini), str(len(sc_locations)), '+', 're-grouped'])) +'\n')  # Ouptut each strand
                        bed_file.write('{0}'.format('\t'.join([str(chrom), str(mini), str(maxi), str(maxi - mini), str(len(sc_locations)), '-', 're-grouped'])) +'\n')  # For annotation purposes
                else:
                    bed_file.write('{0}'.format('\t'.join([str(chrom), str(mini), str(maxi), str(maxi - mini), str(df_chrom.clip_location.count()), '+', 'na'])) +'\n')
                    bed_file.write('{0}'.format('\t'.join([str(chrom), str(mini), str(maxi), str(maxi - mini), str(df_chrom.clip_location.count()), '-', 'na'])) +'\n')
    return bed_f    # Return bed file for annoation by intersection with gff
            
def group_by_average_distance(lst):
    """ Calcualte average distance between sequential bases then group those wiht difference < the average """
    sorted_clip_locations = sorted(lst) # Sort clip locations
    diff = [b - a for a,b in zip(*[iter(sorted_clip_locations)] * 2)]   # List of differences between i values
    avg = sum(diff) / len(diff)                                         # Mean
    grouped = [[sorted_clip_locations[0]]]                              # List of lists for appending groups
    for position in sorted_clip_locations[1:]:                          # Iterate through everything but the first one
        if (position - grouped[-1][0]) < avg:                           # If the difference is less than the average
            grouped[-1].append(position)                                # Add the new position to current array
        else:
            grouped.append([position])                                  # Otherwise start a new group
    return grouped
       
def combine_grouped_lists(grouped_lst):
    """ Group sc locations if they are within n distance of each other """
    # If a gap is greater than n, then that group is finished and everything needs to shift over
    combined = [ grouped_lst[0] ] # Include the first one in this new list to compare to...
    for group in grouped_lst[1:]: # Every remaining list
        for position in group:            # Iterate over sc locations
            if position - combined[-1][0] < 2000: # Test gap between lowest from last combined group and current position (remember sorted in ascending)
                combined[-1].append(position) # Add position to the current group if close enough
            else:
                combined.append([position]) # Else add ass a new array
    return combined
    


