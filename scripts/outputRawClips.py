# Output a subset of all of the individual eraligned, clipped read data
# These reads are those which align to the known insertion sequecne

import pysam, os
import pandas as pd
import numpy as np

def output_raw_clips(KI_obj):
    """ Output all raw realigned reads to file """
    raw_clip_out = KI_obj.realigned_bam.split('/')[-1].split('.')[0] + '.rawreadSummary'    # Generate Raw read file name
    return output_realigned(KI_obj.realigned_bam, KI_obj.pool, raw_clip_out)                # Output all re_aligned reads for maunal curation
    
def output_realigned(realigned_bam, pool, out):
    """ Outputs the raw re_aligned reads summary by pool """
    outdir = os.getcwd() + '/outputs'
    if not os.path.exists(outdir + '/clipped_reads/' + pool):       # One clipped read directory for each pool
        os.makedirs(outdir + '/clipped_reads/' + pool)              # Make output directory if not present
    raw_summary = outdir + '/clipped_reads/' + pool + '/' + out     # Generate raw summary file name
    with open(raw_summary, 'w') as ouf:                             # Open raw summary name
        ouf.write('{0}'.format('\t'.join(['read_id', 'read_start', 'chrom', 'cigar', 'clip_location', 'strand', 'seq'])+'\n')) # Output headers to raw read file
        bam = pysam.AlignmentFile(realigned_bam, "rb")              # Open the realigned bam file for reading
        for read in bam.fetch(until_eof=True):                      # Iterate through all the reads
            if read.cigarstring != 'None':                          # Only include those with cigar strings (to inform clip location)
                output = parse_raw_output(read, bam)                # Parse each read to return a list of data for writing
                if output:                                          # Excludes reads ambiguously mapped reads as output returns False if chrom is '-1'
                    ouf.write('{0}'.format('\t'.join(output)+'\n')) # Write specific read data to file based on generate_raw_output_list
        bam.close()                                                 # Close bam file
    return raw_summary                                              # Return file path to raw read output

def parse_raw_output(read, bam):
    """ As long as the read is mapped '-1' returned when not sure of chromosome """
    if read.reference_id != -1:                                     # -1 indicates the reference is unknown
        if read.is_reverse:                                         # Determine strand 
            strand = '-'                                            # Reverse is negative
        else:
            strand = '+'                                            # Not reverse is positive
        clipped_pos = str(clip_location_from_cigar(read))           # Identify clip position, return list of outputs
        return [read.query_name, \
                str(read.reference_start + 1 ), \
                str(bam.get_reference_name(read.reference_id)), \
                read.cigarstring, \
                clipped_pos, \
                strand, \
                read.query_sequence]
    else:
        return False
                    
def clip_location_from_cigar(read):
    """ Return coordinates of clips """
    read_pos = read.get_reference_positions(full_length=True)   # Get all of the mapped coordinates, None means clipped
    print(read.id)
    print(read_pos)
    if None in read_pos:                                        # Then there is some sort of clipping 
        if read_pos[0] == None and read_pos[-1] == None:        # Clipped both ends
            return '--'                                         # Return a '--' rather than a position
        elif read_pos[0] == None:                               # Start is clipped. Explicit exclude end to avoid error
            return [pos for pos in read_pos if pos is not None][0] + 1  # Return first +1 as sam/bam are 1 based 
        elif read_pos[-1] == None:                                      # End is clipped. 
            return [pos for pos in read_pos if pos is not None][0] + 1  # Return last +1 as sam/bam are 1 based
    else:
        return '-'                                              # Return '-' if not clipped Return read start if it's not clipped

def output_filtered_annotated_clips(KI_obj):
    summary_file = os.getcwd() + '/outputs/insertions/' + KI_obj.pool +'/' + KI_obj.pool + '.insertions'
    headers = ['chrom',\
              'min_clip_pos',\
              'max_clip_pos',\
              'clip_range',\
              'clip_count',\
              'strand',\
              'gene_id',\
              'feat_type',\
              'distance_from_insertion',\
              'feat_start',\
              'feat_stop',\
              'feat_strand',\
              'description',]
    with open(summary_file, 'w') as ouf:
        df = KI_obj.annotation_df[headers]
        df.to_csv(ouf, sep='\t', index=False)
    return ouf

