import pysam, sys, os
from scripts.ReadPair import *

class ParseKI():
    """ One per sample. Outlines hygromycin reads supporting interchromosomal translocation
        Initialises cluster pair objects from ClusterPair"""
    re_cluster_dict = {}        #dictionary to contain each cluster which needs re-clsutering
    complete_bool = False

    def __init__(self, pool, bam, fq1, fq2, region):
        """ Initialisation controls extraction of reads from region and clustering by chromosome """ 
        ### Varaibles provided from input ###
        self.pool = pool        # Pooled sample id
        self.bam = bam          # Aligned pool data
        self.fq1 = fq1          # Forward fastq for hard clip lookup
        self.fq2 = fq2          # Reverse fastq for hard clip lookup
        self.region = region    # Region i.e. hygromycin cassette 'chromosome'
        
        ### Variables generated from processing inputs ###

        self.allRegionReads = []            # All region reads
        self.pairedRegionReads = []         # Region reads w/ pairs
        self.interRegionReads = []          # Region reads w/ interchromosomal pair
        self.rePairedInterRegionReads = {}  # Region reads w/ intechromosomal pair {chrom :[ReadPair_obj, ...]}
        
        self.cluster_levels = []        # Need to change to Dict of cluster levels for each chromosome {chrom : {level : leveln, [cluster_pair_objs]}
        self.KI_soft_clip_fq = ""       # List of all KI region clipped read objects
        self.KI_hard_clip_fq = ""
        self.realigned_bam = ""         # Bam file of clipped reads realigned to genome without KI
        self.realigned_raw_reads = ""   # Summary of realigned reads output with read sequences and softclip locaitons
        self.unannotated_bed = ""       # Bed file of soft clip data within 2kb of each other
        self.annotated_bed = ""         # Bed file annotated by intersection with gff
        self.annotation_df = {}         # Data frame containing all annotated, filtered insertion sites
        self.peptide_fasta = ""         # File path to this pool filtered peptides
        self.insertion_file = ""        # Summary of annotated bed file/annotation_df
        self.border_seqs = ""           # Border sequences for either side of insertions
                
        ### Parse reads from input bam files ###
        self.parseAllRegion()           # Update allRegionReads
        self.parsePairedRegion()        # Update pairedRegionReads
        self.parseInterRegion()         # Update interRegionReads
        self.rePairInterRegion()        # Update rePairedInterRegionReads
        
        #self.excluded_clusters = []  #updated by categoriseclusterinstances
        #self.categorised_clusters = [] #clusters not filtered by total length
        #self.finalised_clusters = []    #clusters to be used for SC analysis will be 'True'
        #self.sub_analysis_list = []  #clusters which require sub cluster analysis
        #self.re_cluster_objects = [] #categorised clusters whcih need re_clustering {cat_pair_n : [re_clust_1, reclust_n]}

    def message(self, message, lst=None):
        """ Prints lenght of list and message """
        if lst:
            print(str(len(lst))+' '+message)
        else:
            print(message)
        
    def parseAllRegion(self):
        """ Extract all region reads """
        with pysam.AlignmentFile(self.bam, 'rb') as bam:                        # Read bam w/ pysam
            self.allRegionReads = [read for read in bam.fetch(self.region)]     # Return all reads from region 
            self.message('reads extracted from ' + self.region, lst=self.allRegionReads)
        
    def parsePairedRegion(self):
        """ Returns list of all paired reads from a bam file within region """
        with pysam.AlignmentFile(self.bam, 'rb') as bam:                                        # Read bam w/ pysam
            self.pairedRegionReads = [read for read in self.allRegionReads if read.is_paired]   # Return paired reads from region   
            self.message('paired reads extracted from ' + self.region, lst=self.pairedRegionReads)
        
    def parseInterRegion(self):
        """ Returns reads with interchromosomal mates from a list of paired pysam read objects """
        region_reads_w_trans = {}
        with pysam.AlignmentFile(self.bam, 'rb') as bam:                # Read bam for chrom lookup
            self.interRegionReads = [read for read in self.pairedRegionReads if bam.get_reference_name(read.next_reference_id) != self.region] # Reads w/ translocated mate
            self.message(self.region + ' reads with interchromosomal mates', lst=self.interRegionReads)

    def rePairInterRegion(self):
        #Missing a few repaired reads - NEED TO RE-work logic here. as not all read accounted for. Outputs are satisfactory at this point.
        #iterate through whole bam file rahter than chromosome? If read is in list of reads associate them...
        #https://groups.google.com/forum/#!topic/pysam-user-group/zvj-REqZodg
        """ Look up pair of interchromosomal region reads and group by chromosome. Chronosome must have > 3 translocated reads """ 
        self.message('Repairing reads from ' +self.region+ '...')
        reads_chrom = self.arrangeByChrom(self.interRegionReads)    # Region reads arranged by chromosome of mate. Used to filter by > 3 interchromosomal from region
        re_paired_reads = {}                                        # Empty for updating to {'chrom' :["one on chrom", "one on region", ...]}
        with pysam.AlignmentFile(self.bam, 'rb') as bam:            # Read bam for chrom lookup
            for chrom, region_reads in reads_chrom.items():         # Iterate through chromosome
                if len(region_reads) > 3:                           # Only lookup mates if there are more than 3 #### FILTER POINT HERE ####
                    inter_read_lookup_dict =  {read.qname : read for read in bam.fetch(chrom)} #Parse all reads into dict by chromosome {read _id : read}
                    self.checkQnameSuffix(region_reads[0].qname)    # Check first read does not have suffix preventing lookup match
                    for read in region_reads:                       # Iterate through region reads
                        if read.query_name in inter_read_lookup_dict.keys():                     # Look up region read id in inter read lookup dict
                            this_pair = ReadPair(read, inter_read_lookup_dict[read.query_name])  # Create ReadPair object if matches
                            try:
                                re_paired_reads[chrom].append(this_pair)                    # Append read to value list if chrom is already a key
                            except:
                                re_paired_reads[chrom] = [this_pair]                        # Otherwise create new key, value list
        self.rePairedInterRegionReads = re_paired_reads                     # Assign repaired reads to KI object
        all_reads = []
        for k,v in self.rePairedInterRegionReads.items():                   # Count numbers of paired reads for user message
            all_reads.append([i.region_read for i in v])
        all_reads = [read for sublist in all_reads for read in sublist]
        self.message('Reads sucessfully paired for cluster analysis', all_reads)           
        self.message('Putitaive insertions on chromosomes ' + str(self.rePairedInterRegionReads.keys()))    # Print chromosomes being considered as having insertions

    def arrangeByChrom(self, reads):
        """ Arranges a list of reads with interchromosomal pairs by chromosome of pair """
        reads_by_chrom = {}                                             # Empty dict for updating
        with pysam.AlignmentFile(self.bam, 'rb') as bam:                # Read bam for chrom lookup
            for read in self.interRegionReads:                          # Iterate region reads w/ interchromosomal pair
                chrom = bam.get_reference_name(read.next_reference_id)  # Chrom number
                try:
                    reads_by_chrom[chrom].append(read)                  # Append read to value list if chrom is already a key
                except:
                    reads_by_chrom[chrom] = [read]                      # Otherwise create new key, value list
        return reads_by_chrom                                           # {'chrom' : [reads w/ interchromosomal mate]}

    def checkQnameSuffix(self, reads):
        """ Checks the firs read doesn't have an orientation suffix """
        assert (not reads[0].endswith('/1')), 'WARNING - Read name suffixed with /1 or /2 to determine which read they are. Remove these or create handler in ParseKID.rePairInterRegion()'
        assert (not reads[0].endswith('/2')), 'WARNING - Read name suffixed with /1 or /2 to determine which read they are. Remove these or create handler in ParseKID.rePairInterRegion()'
        
        