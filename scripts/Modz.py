from sklearn.cluster import KMeans
import numpy as np

class ModZ():
    """ Models creation of Modz inlier and outlier list
        outliers could be outliers or separate insertion """
        
    inliers = []
    outliers = []  
        
    def __init__(self, cluster):   #, inliers, outliers):
        self.cluster = cluster
        self.cluster_range = self.rang(cluster)
        self.classifier(cluster)
        self.inlier_count = str(len(self.inliers))
        self.inlier_range = str(self.rang(self.inliers))
        self.outlier_count = str(len(self.outliers))
        self.outlier_range = str(self.rang(self.outliers))
 
    def classifier(self, cluster):
        """ Modified z scores for each cluster in the pair
            returns read positions for retainers (ins) and outliers (outs) """
        if len(cluster) > 1:
            data = np.array(cluster, dtype=float)             # Create array of floats
            med = np.median(data)                               # Median of array
            mad = np.median([np.abs(i - med) for i in data])    # Median absolute deviation
            mod_z = [0.6745 * (i - med)/mad for i in data]    
            self.inliers = [int(data[i]) for i in self.noneCatch(np.where(np.abs(mod_z) < 3.5)) if i != None]    #inliers if mod_z < 3.5   
            self.outliers = [int(data[i]) for i in self.noneCatch(np.where(np.abs(mod_z) >= 3.5)) if i != None]  #outliers if mod_z < 3.5

    def noneCatch(self, mod_z_tup):
        """ Prevents empty list i.e. tuple[0] causing none type error """
        if len(mod_z_tup[0]) != 0:
            return mod_z_tup[0].tolist()
        else:
            return [None]
            
    def rang(self, lst):
        """ Calculate distance between highest and lowest coordinate in cluster pair """
        if len(lst) >= 1:               # Check there is a max and a min
            return max(lst)-min(lst)
        else:
            return lst