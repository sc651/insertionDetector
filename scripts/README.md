# CategoriseClusters logic
# Overview

https://www.biostars.org/p/17688/
If the query frame is positve then the supplied query seq was matched.  
If the query frame is negative then the reverse complement query seq was matched.

The following flowchart outlines the logic and thresholds used to categorise each cluster pair (CP)  
CP = ClusterPair object
CCP = CategoriseClusterPair object
RC = Read count
D = Distance between read starting positions
SC = soft clip
K = K-means cluster

```mermaid
graph TD;
A["Initialise CategoriseclusterPair object (CCP) for each (CP)"]--CP Total RC < 4-->B[Remove CP - not enough reads];
A["Initialise CategoriseclusterPair object (CCP) for each (CP)"]--CP Total RC >= 4-->C[CP pass - continue tests];
C[CP pass - continue]-->D[Test K1, K2 low RCs];
D[Test K1, K2 low RC combinations]-->E[RC=0 and RC=4];
D[Test K1, K2 low RC combinations]-->F[RC=1 and RC=3];
D[Test K1, K2 low RC combinations]-->G[RC=2 and RC=2];
E[RC=0 and RC=4]--D>6000 excluded-->H[If pass low RC - continue];
F[RC=1 and RC=3]--D>6000 excluded-->H[If pass low RC - continue];
G[RC=2 and RC=2]--D>6000 excluded-->H[If pass low RC - continue];
H[If pass low RC - continue]-->I[One K cluster remains];
H[If pass low RC - continue]-->J[Both K clusters remain];
I[One K cluster remains]-->K[K cluster distance tests];
J[Both K clusters remain]-->K[K cluster distance tests];
K[K cluster distance tests]--D<6000-->L[Use K clusters for SC analysis];
K[K cluster distance tests]--D>6000-->M[Analyse sub clusters for D < 6000];
