#contains code required for re-clustering

#THIS CODE MAY HAVE BECOME OBSOLETE DUE TO PREVIOUS FILTERING

def update_obj(obj, filter_code, *args):
    """ Update object *args with filter code """
    for arg in args:
        setattr(obj, arg, filter_code)

def determine_re_cluster_prediction(cat_cluster):
    """ Re clustering should stop when no Function to determin re-clustering """
    both_categories = ['UK2K1sub', 'UK1K2sub']      #categories where the outcome of Knsub is to be considered in context of other Kn
    if cat_cluster.prediction in both_categories:
        print('---------')
        print(cat_cluster.prediction)
        if cat_cluster.K1 == True:       #Determine which K cluster to use by isolating True as previously assigned
            #test_ lenghts if sub clusters are < 4 - discount
            S1_data = cluster.K2_data.ModZpair['c1']
            S2_data = cluster.K2_data.ModZpair['c2']
            S1_count = test_count_range(s1_data['c1_ins'], s1_data['c1_in_range'])
            S2_ count = test_count_range(s2_data['c2_ins'], s2_data['c2_in_range'])
            if S1_count and s2_count:
     
            #setattr(cat_cluster, 'sub_prediction', known_k_sub_cat(cat_cluster, cat_cluster.K2subclust_dist_cat, 1, 2, 2)) #use K1 data and sub cluster of other              
        elif cat_cluster.K2 == True:
            S1_data = cluster.K1_data.ModZpair['c1']
            S2_data = cluster.K1_data.ModZpair['c2']
            
            
            #setattr(cat_cluster, 'sub_prediction', known_k_sub_cat(cat_cluster, cat_cluster.K1subclust_dist_cat, 2, 1, 1)) #use K2 data and sub cluster of other            
        else:
            print('Error - determine re cluster - K1 or K2 should be True')
        print(cat_cluster.sub_prediction)            
    #elif cat_cluster.prediction == 'K1sub':
    #    single_sub_cat 
    #    
    #elif cat_cluster.prediction == 'K2sub':
    #     single_sub_cat
    #    pass        
    #elif cat_cluster.prediction == 'K1K2sub':  #Kn is false for both - need to determine sub cluster status 
    #    pass  
    
    return #True or False for re_cluster or not


def test_count_range(sn_l, sn_r):
    """ returns false if <= 4 reads (snl) and > 6000kb sn(r) """
    if len(sn_l) <= 4 and sn_r > 6000:
        False #immediately discard from re_clutering
    elif len(sn_l) > 4 and sn_r > 6000:
        True 
               
    
def known_k_sub_cat(cluster, attr_str, K, subn, other_k, single=False):
    """ when you know one K cluster is to be used and ruling out sub clusters of other K """
    print('RS_' + str(subn) + '_LOW')
    if attr_str == 'RS_' + str(subn) + '_LOW': 
        return 'UK' + str(K) + 'K' + str(other_k) + 'S' + str(subn) #two insertions UKn1Kn2+Sn   
    elif attr_str == 'RS_' + str(subn) + '_HIGH':
        #if length/range of sub cluster is same length as K cluster then filter this K cluster as noise
        if test_sub_length_and_range(other_k, subn):
            return 'UK' + str(K) + 'K' + str(other_k) + 'S' + str(subn) #use the sub cluster 
        else:
            #test count if < 10 then don't re_cluster
            if test_count(other_k, subn):
                return 'RCK' + str(other_k) + 'S' + str(subn) #recluster
            else:
                return 'UK' + str(K) #don't use the sub cluster
    elif attr_str == 'RS1S2SNHIGH':
        return 'UK' + str(K) + 'K' + str(other_k) +'S1S2'   #Three total events, each sub cluster is an event 
    elif attr_str == 'RS1S2HIGH':                           #Noise in both sub clusters, check lengths/dif, likely need to re-cluster
        test_count(other_k, )
        return 'RCK' + str(other_k) + 'S1S2'
    elif attr_str == 'RS1LOWS2HIGH':                        #Noise in S2 use S1, check length of S2, re-cluster S2 if required
        test_sub_length(other_k, 2)         #subn is by default 2 here as 1 is low
        pass
    elif attr_str == 'RS1HIGHS2LOW':                        #Noise in S1 use S2, check length of S1, re-cluster S1 if required
        pass

def test_sub_length_and_range(other_k, subn):
    if other_k == 1:
        K1_length = len(cluster.K1_data['clusters'])
        if subn == 1:
            sub_length = len(cluster.K1_data.ModZpair['c1']['c1_ins'])
            sub_range = cluster.K1_data.ModZpair['c1']['c1_in_range']
        elif subn == 2:
            sub_length = len(cluster.K1_data.ModZpair['c2']['c2_ins'])
            sub_range = cluster.K1_data.ModZpair['c2']['c2_in_range']
        if K1_length == sub_length:
            return False #False to result in the removal of this K clsuter - all outliers!
        else:
            if int(sub_range) < 18000 and sub_range != 0:    #test length < 20Kb.... COULD ALSO TEST TO REMOVE < 4 READS SUB_COUNT >= 4
                return True
            else:
                return False # remove because the sub clsuter is likely noise
    elif other_k == 2:  
        K2_lenghth = len(cluster.K2_data['clusters'])
        if subn == 1:
            sub_length = len(cluster.K2_data.ModZpair['c1']['c1_ins'])
            sub_range = cluster.K2_data.ModZpair['c1']['c1_in_range']
        elif subn == 2:
            sub_length = len(cluster.K2_data.ModZpair['c2']['c2_ins'])
            sub_range = cluster.K2_data.ModZpair['c1']['c2_in_range']
        if K1_length == sub_length:
            return False
        else:
            if int(sub_range) < 18000 and sub_range != 0:    #test length < 20Kb....
                return True
            else:
                return False

def test_count(other_k, subn):
        if other_k == 1:
            if subn == 1:
                sub_length = len(cluster.K1_data.ModZpair['c1']['c1_ins'])
            elif subn == 2:
                sub_length = len(cluster.K1_data.ModZpair['c2']['c2_ins'])
            if sub_length <= 10:
                return False #False to result in the removal of this K clsuter - not enough fo re_clustering
            else:
                return True
        if other_k == 2:
            if subn == 1:
                sub_length = len(cluster.K2_data.ModZpair['c1']['c1_ins'])
            elif subn == 2:
                sub_length = len(cluster.K2_data.ModZpair['c2']['c2_ins'])
            if sub_length <= 10:
                return False #False to result in the removal of this K clsuter - not enough fo re_clustering
            else:
                return True
 
def single_sub_cat():
    """ when there is only one sub cluster analysis for a single K """


def update_finalised_catgories(cluster):
    """ """



#--------------------------------------------------------------    
def re_cluster_loop(): #Clusterpair needing re_clsutering):
    """ controller loop for re_clustering """
    complete = False
    while not complete:
        pass
        #call kmeans cluster
        #initalise cluster object directly
        #call categorise_re_cluster - if False complete = True - becuse the clusters have been excluded or don't need - reclustering
        #test the major K clusters

#code to re-do a lcuster without initialising Hyg parse object        
#def kmeans_cluster():
#    read_starts = sorted([read.reference_start for read in reads], key=int) #list of read start positions sorted numerically
#    km = KMeans(n_clusters=2)
#   if len(read_starts) > 1:                                                #Iterate if mroe than one read mapped 
#        data = np.asarray(read_starts).reshape(-1,1) #[:,None]              #reshape data for clustering
#        fitted_data = km.fit(data)                                          #fit the data to two clusters
#        this_re_clsuter = [[x for i,x in enumerate(read_starts) if fitted_data.labels_[i] == cluster_n] for cluster_n in range(km.n_clusters)]
        
#def categorise_re_cluster(cluster_object):
    #float(id) += 0.1 #add 0.1 to id indicatig the re-clsuter level
    #print(id)
#    categorised_cluster = CategoriseClusterPair(cluster_object, id)# initalise and test total cluster length each cluster in each pool gets an integer id 
#    if categorised_cluster.filter == True:                  #initialised will return .filter=True if the total reads are < 4
#        return False                                        #return False to break the re-clsuter while loop
#    else:                                                   #continue analysis
#        categorised_cluster.categorise_k_clusters()         #update: TRD for K1,K2; difference (D) category, SD category, distance (R) category
#        if categorised_cluster.K1 == 'TRD' and categorised_cluster.K2 == 'TRD':
#            h.excluded_clusters.append(categorised_cluster) #exclude based on both kclusters with 2 reads which are far apart - boht 'TRD'
#        else:
#            h.categorised_clusters.append(categorised_cluster)#append the catgorised clusters to a list

    
    
    
    
    
    
    
    
    
    