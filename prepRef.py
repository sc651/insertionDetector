#Helper script to add the known insertoin sequence to the reference prior to alignment
import argparse as ap
import subprocess

def concat_fasta(fasta_1, fasta_2, output):
    """ Invokes cat through subprocess to combine two fasta files. """
    cmd = ['cat', fasta_1, fasta_2]
    ouf = open(output, 'w')
    subprocess.call(cmd, stdout=ouf)
    ouf.close()
    
def index_fasta(fasta, samtools=None):
    """ uses samtools to create a fasta index """
    if samtools:
        cmd = [samtools, 'faidx', fasta]
        subprocess.call(cmd)
    else:
        cmd = ['samtools', 'faidx', fasta]
        subprocess.call(cmd)

def create_seq_dict(fasta):
    ref = 'R=' + fasta 
    dict = 'O=' + fasta + '.dict'
    cmd = ['java', '-jar', 'CreateSequenceDictionary', ref, dict]
    subprocess.call(cmd)
    
def create_bwa_idx(fasta):
    cmd = ['bwa', 'index', '-a', 'bwtsw', fasta]
    subprocess.call(cmd)  
        
if __name__ == "__main__":
    parser = ap.ArgumentParser()
    parser.add_argument("-r", "--reference_fasta", help="Reference fasta file to add the known insertion sequence to")
    parser.add_argument("-o", "--output", help="File name of the combined output file")
    parser.add_argument("-ki", "--known_insertion_seq", help="A fasta file of the known insertion DNA sequence to be added to the reference")
    parser.add_argument("-s", "--samtools", default=None, help="path to samtools") # Check None isnt returning true
    # Add a config file for all the dependencies which can be used in the main program too 
    args = parser.parse_args()
    concat_fasta(args.reference_fasta, args.known_insertion_seq, args.output)
    index_fasta(args.output, samtools=args.samtools)
    create_seq_dict(args.output)
    create_bwa_idx(args.output)
